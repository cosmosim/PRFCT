#!/bin/bash
clear
echo "prfct redeploying..."
git pull
cd frontend
npm install
npm run build
cd ../server
npm i
cd ..
docker rm $(docker ps -a -q)
docker rmi $(docker images -q)
docker-compose up
